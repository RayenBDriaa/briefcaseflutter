// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:briefcase/icons/symicons.dart';
import 'package:flutter/material.dart';

class Brief2023 extends StatefulWidget {
  const Brief2023({Key? key}) : super(key: key);

  @override
  State<Brief2023> createState() => _HomeState();
}

class _HomeState extends State<Brief2023> {
  int _selectedIndex = 0;
  NavigationRailLabelType labelType = NavigationRailLabelType.all;
  bool showLeading = false;
  bool showTrailing = false;
  double groupAligment = -1.0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Row(children: [
      
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 122,
                width: double.infinity,
                color: const Color(0xFFAECC9B),
                  child: _selectedIndex == 1
                      ? Container(
                          margin: const EdgeInsets.only(
                              top: 20, left: 15, right: 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              Text(
                                "Dialog Presentations",
                                style: TextStyle(
                                    fontSize: 28.75,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFFFFFFFF),
                                    letterSpacing: 1),
                              ),
                              SizedBox(
                                height: 2,
                      ),
                      Text(
                                "All the answers you need to your clients most common questions.",
                                style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontSize: 18.4,
                                    color: Colors.white60),
                      ),
                    ],
                  ),
                        )
                      : Container(
                          margin: const EdgeInsets.only(
                              top: 20, left: 15, right: 0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: const [
                              Text(
                                "Presentations 2023",
                                style: TextStyle(
                                    fontSize: 28.75,
                                    fontWeight: FontWeight.w400,
                                    color: Color(0xFFFFFFFF),
                                    letterSpacing: 1),
                              ),
                              SizedBox(
                                height: 2,
                              ),
                              Text(
                                "All the answers you need to your clients most common questions.",
                                style: TextStyle(
                                    fontStyle: FontStyle.italic,
                                    fontSize: 18.4,
                                    color: Colors.white60),
                              ),
                            ],
                          ),
                        )
              ),
              Expanded(
                child: Container(
                  color: const Color(0xFFF3FCED),
                ),
              ),
            ],
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  height: 317,
                  child: Stack(children: [
                    NavigationRail(
                      backgroundColor: const Color(0xFF79B3DE),
                      minWidth: 286,
                      extended: false,
                      selectedIndex: _selectedIndex,
                      groupAlignment: groupAligment,
                      onDestinationSelected: (int index) {
                        setState(() {
                          _selectedIndex = index;
                        });
                      },
                      labelType: labelType,
                      leading: showLeading
                          ? FloatingActionButton(
                              elevation: 10,
                              onPressed: () {
                                // Add your onPressed code here!
                              },
                              child: const Icon(Icons.add),
                            )
                          : const SizedBox(),
                      trailing: showTrailing
                          ? IconButton(
                              onPressed: () {
                                // Add your onPressed code here!
                              },
                              icon: const Icon(Icons.more_horiz_rounded),
                            )
                          : const SizedBox(),
                      destinations: <NavigationRailDestination>[
                        NavigationRailDestination(
                          label: Text(''),
                          icon: Icon(
                            Symicons.Icons_old_dialog_icon_120x120,
                            color: Colors.white12,
                          ),
                          selectedIcon: Icon(
                            Symicons.Icons_old_dialog_icon_120x120,
                            color: Colors.white12,
                          ),
                        ),
                        NavigationRailDestination(
                          icon: Icon(Icons.bookmark_border),
                          selectedIcon: Icon(Icons.book),
                          label: Text('Second'),
                        ),
                        NavigationRailDestination(
                          icon: Icon(Icons.star_border),
                          selectedIcon: Icon(Icons.star),
                          label: Text('Third'),
                        ),
                      ],
                    ),
                    Container(
                      height: 317,
                      color: Colors.red,
                      child: InkWell(
                        onTap: (() {
                          setState(() {
                            _selectedIndex = 1;
                          });
                          print(_selectedIndex);
                        }),
                        child: Image.asset(
                          "assets/images/SideBarDialog.png",
                          fit: BoxFit.fill,
                        ),
                      ),
                    )
                  ]),
                ),
              ],
            ),
          ],
        ),
      ]),
    );
  }
}
