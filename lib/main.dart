import 'package:briefcase/brief/briefCase.dart';
import 'package:briefcase/brief/brief_2023.dart';
import 'package:briefcase/home/SettingsDialog.dart';
import 'package:bitsdojo_window/bitsdojo_window.dart';
import 'package:briefcase/home/symhome.dart';
import 'package:briefcase/testRoutes.dart';
import 'package:flutter/material.dart';

import 'brief/briefMain.dart';
import 'package:splash_view/splash_view.dart';


const symColor = Color(0xFF1e4cab);

void main() {
  
  runApp(const AppBarApp());
  doWhenWindowReady(() {
    const initialSize = Size(1280, 960);
    appWindow.minSize = initialSize;
    appWindow.size = initialSize;
    appWindow.alignment = Alignment.center;
    appWindow.show();
  });
}

class AppBarApp extends StatelessWidget {
  const AppBarApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Symmetryk",
      home:  SplashView(
        
        backgroundColor: symColor,
        showStatusBar: true,
        backgroundImageDecoration:BackgroundImageDecoration(image: AssetImage("assets/images/SymSplash.png")) ,
        done: Done(BriefMain()),
      ),
       
      routes: {
        "/test": (BuildContext context) {
          return const TestRouts();
        },
        "/r":(BuildContext context) {
          return const BriefMain();
        },
        "/s":(BuildContext context)  {
          return const SymHome();
        },
        "/briefcase":(BuildContext context)  {
          return const Briefcase();
        },
        "/settings":(BuildContext context)  {
          return MyApp();
        },
        "/briefexemlpe": (BuildContext context) {
          return const Brief2023();
        },
      },
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Open Sans',
        textTheme: Theme.of(context).textTheme.apply(
              bodyColor: Colors.white,
              displayColor: Colors.white,
            ),
        colorSchemeSeed: const Color(0xff6750a4),
        useMaterial3: true,
      ),
      
    );
  }
}


